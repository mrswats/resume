# Ferran Jovell Megias CV

[![pipeline status](https://gitlab.com/mrswats/ferranjovellcv/badges/main/pipeline.svg)](https://gitlab.com/mrswats/ferranjovellcv/-/commits/main)

Hello! My name is Ferran Jovell Megias and here you will find my resume.
You can download the PDF version in this [_**link**_](https://gitlab.com/mrswats/resume/-/jobs/artifacts/main/raw/FerranJovellMegiasCV.pdf?job=pdf)

    https://gitlab.com/mrswats/resume/-/jobs/artifacts/main/raw/FerranJovellMegiasCV.pdf?job=pdf

The old version (and outdated) version of the CV can be found [here](https://gitlab.com/mrswats/resume/-/jobs/artifacts/main/raw/OldFerranJovellMegiasCV.pdf?job=old-pdf).
